FROM openjdk:8-jre-slim
WORKDIR /workspace
COPY target/api-test-devops-*.jar app.jar
EXPOSE 9966
ENTRYPOINT [ "java", "-jar", "/workspace/app.jar" ]